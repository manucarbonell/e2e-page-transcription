import xml.etree.ElementTree as ET
from PIL import Image,ImageDraw
import fnmatch
import os
import sys
import shutil
import numpy as np
from shutil import copyfile
import json
from PIL import Image
MAX_IMAGES=500
MAX_REGIONS=100
if len(sys.argv)==2:
    gt_dir=sys.argv[1]
else:
    print "Usage:\n python ",sys.argv[0]," [pagexml gt dir]"
    sys.exit()
    # gt_dir='/home/mcarbonell/Documents/DATASETS/gt-ancestry'

def create_input_dir_and_ids():
    #Put all gt images in a single directory with their id + extension as filename.

    image_ids=[]
    matches = []
    image_id=0
    basewidth =2048
    for root, dirnames, filenames in os.walk(gt_dir): ## Folder where we have the xmls of the ancestry_gt
        for filename in filenames:
            if fnmatch.fnmatch(filename, '*.jpg') or fnmatch.fnmatch(filename, '*.png'):
            # fnmatch.filter(filenames, '*.jpg'):
                image_id=image_id+1

                extension = os.path.splitext(filename)[1]
                print "Process im with id",image_id

                if image_id>MAX_IMAGES:
                    break
                image_ids.append(os.path.splitext(filename)[0])
                # matches.append(os.path.join(root, os.path.splitext(filename)[0]+'_gt.xml'))  #FOR ANCESTRY
                matches.append(os.path.join(root, os.path.splitext(filename)[0]+'.xml'))  # FOR ESPOSALLES
                shutil.copy(os.path.join(root,filename),os.path.join(image_dir_out,str(image_id)+extension))
        if image_id>MAX_IMAGES:
            break
    return image_ids,matches

image_dir_out='gt-all'

if not os.path.exists(image_dir_out):
    os.mkdir(image_dir_out)

image_ids,matches=create_input_dir_and_ids()
filename_out='region_annotations.json'
#
# #DEBUG
# matches=[]
# for i in range(6):
#     matches.append(os.path.join('/home/mcarbonell/from_jmas/train','Volum_069_Registres_0026.xml'))
# #DEBUG

images=[]
def gen_images_json():
    n_region=0
    n_image=0
    for m in matches:
        n_image=n_image+1
        vals = m.split('/');
        filename_in = vals[-1]
        filename_in_wext = filename_in.split('.')[0]
        image={}
        image['id']=image_ids.index(filename_in_wext)+1 # FOR ESPOSALLES
        image['regions']=[]
        im=Image.open(os.path.join('gt-all',str(image['id'])+'.png'))
        draw = ImageDraw.Draw(im)

        tree = ET.parse(m);

        n_region_image=0

        for line_fields in tree.iter('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextLine'):
            region={}
            box_transcription=""
            n_region=n_region+1
            n_region_image=n_region_image+1
            region['id']=n_region
            for line_field in line_fields:
                if line_field.tag=='{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Coords':
                    if line_field.attrib.get('points'):
                        xy=line_field.attrib['points'].split(' ')
                        x0=int(xy[0].split(',')[0])
                        y0=int(xy[0].split(',')[1])
                        x1=int(xy[2].split(',')[0])
                        y1=int(xy[2].split(',')[1])
                        region['width']=x1-x0
                        region['height']=y1-y0
                        region['x']=x0
                        region['y']=y0
                        draw.rectangle([x0, y0, x1, y1],fill=None)

                if line_field.tag=='{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Word':
                    for word_field in line_field:
                        for element in word_field:
                            box_transcription=box_transcription+" " +element.text

            region['phrase']=box_transcription
            draw.text([x0, y0],box_transcription,fill='black')
            image['regions'].append(region)

            if n_region_image>=MAX_REGIONS:
                break
        images.append(image)
        im.show()
        print image
        raw_input()
    return images



images=gen_images_json()

fileout = open(filename_out,'w');
fileout.write(json.dumps(images))
fileout.close()
