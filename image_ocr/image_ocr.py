# -*- coding: utf-8 -*-
import os
import itertools
import codecs
import re
import datetime
import cairocffi as cairo
import editdistance
import numpy as np
from scipy import ndimage
import pylab
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation
from keras.layers import Reshape, Lambda
from keras.layers.merge import add, concatenate
from keras.models import Model
from keras.layers.recurrent import GRU
from keras.optimizers import SGD
from keras.utils.data_utils import get_file
from keras.preprocessing import image
import keras.callbacks
import EsposallesCompetition
from EsposallesCompetition import EsposallesDataset
from IAM_input import IAM_input


from PIL import Image
import tensorflow as tf
OUTPUT_DIR = 'image_ocr'

# character classes and matching regex filter
regex = r'^[a-z ]+$'
alphabet = u'abcdefghijklmnopqrstuvwxyz '
iam=IAM_input()

np.random.seed(55)
batch_size=iam.batch_size
max_word_len=iam.max_seq_len
absolute_max_string_len=iam.max_seq_len
# this creates larger "blotches" of noise which look
# more realistic than just adding gaussian noise
# assumes greyscale with pixels ranging from 0 to 1
E=EsposallesDataset()
def speckle(img):
    severity = np.random.uniform(0, 0.6)
    blur = ndimage.gaussian_filter(np.random.randn(*img.shape) * severity, 1)
    img_speck = (img + blur)
    img_speck[img_speck > 1] = 1
    img_speck[img_speck <= 0] = 0
    return img_speck


# paints the string in a random location the bounding box
# also uses a random font, a slight random rotation,
# and a random amount of speckle noise


# Translation of characters to unique integer values
def text_to_labels(text):
    ret = []
    for char in text:
        ret.append(alphabet.find(char))
    return ret


def save_images_batch(batch,names):
    for example in range(batch.shape[0]):
        im=Image.fromarray(255*batch[example,:,:,0])
        im=im.convert('RGB')
        print "IMAGE SIZE",im.size
        im.save(names[example]+'.jpg')


# Reverse translation of numerical classes back to characters
def labels_to_text(labels):
    ret = []
    # print labels
    for c in labels:
        if c == len(alphabet):  # CTC Blank
            ret.append("")
        elif c==-1:
            continue
        else:
            # print c
            ret.append(alphabet[c])
    return "".join(ret)


# only a-z and space..probably not to difficult
# to expand to uppercase and symbols

def is_valid_str(in_str):
    search = re.compile(regex, re.UNICODE).search
    return bool(search(in_str))


# Uses generator functions to supply train/test with
# data. Image renderings are text are created on the fly
# each time with random perturbations

def get_batch(img_w,downsample_factor):#, index, size, train):
    # width and height are backwards from typical Keras convention
    # because width is the time dimension when it gets fed into the RNN

    ims,trans=iam.get_batch()
    # print trans
    X_data=ims
    # print "GET BATCH",ims.shape,trans
    source_str=[]
    labels=np.full((batch_size,max_word_len),-1)
    label_length=np.zeros((batch_size,1))
    input_length=np.zeros((batch_size,1))
    for example in range(batch_size):

        # labels[example,:len(trans[example])]=trans[example]

        labels[example,:len(trans[example][:absolute_max_string_len])]=trans[example][:absolute_max_string_len]
        # print labels.shape
        # raw_input()
        label_length[example]=len(trans[example])
        img_w=ims[example].shape[1]
        input_length[example]= img_w // downsample_factor - 2

        source_str.append(labels_to_text(trans[example]))


    inputs = {'the_input': X_data[:batch_size,:,:,:],
              'the_labels': labels,
              'input_length': input_length,
              'label_length': label_length,
              'source_str': source_str  # used for visualization only
              }
    # for k,v in inputs.iteritems():
    #     try:
    #         print k,v.shape
    #     except:
    #         print v
    # raw_input()
    outputs = {'ctc': np.zeros([batch_size])}  # dummy data for dummy loss function

    return (inputs, outputs)

def get_output_size():
    return len(alphabet) + 1


# the actual loss calc occurs here despite it not being
# an internal Keras loss function

def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    # the 2 is critical here since the first couple outputs of the RNN
    # tend to be garbage:
    y_pred = y_pred[:, 2:, :]

    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)


# For a real OCR application, this should be beam search with a dictionary
# and language model.  For this example, best path is sufficient.

def decode_batch(test_func, word_batch):
    out = test_func([word_batch])[0]
    ret = []
    for j in range(out.shape[0]):
        out_best = list(np.argmax(out[j, 2:], 1))
        out_best = [k for k, g in itertools.groupby(out_best)]
        outstr = labels_to_text(out_best)
        ret.append(outstr)
    return ret


def train(run_name, start_epoch, stop_epoch, img_w):
    # Input Parameters
    E=EsposallesDataset()
    img_h = 64
    words_per_epoch = 16000
    val_split = 0.2
    val_words = int(words_per_epoch * (val_split))

    # Network parameters
    conv_filters = 32
    kernel_size = (5, 5)
    pool_size = 2
    time_dense_size = 64
    rnn_size = 512

    if K.image_data_format() == 'channels_first':
        input_shape = (1, img_h, img_w)
    else:
        input_shape = (img_h, img_w, 1)

    downsample_factor=(pool_size ** 2)

    act = 'relu'
    input_data = Input(name='the_input', shape=input_shape, dtype='float32')

    inner = Conv2D(conv_filters, kernel_size, padding='same',
                   activation=act, kernel_initializer='he_normal',
                   name='conv1')(input_data)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max1')(inner)
    inner = Conv2D(conv_filters, kernel_size, padding='same',
                   activation=act, kernel_initializer='he_normal',
                   name='conv2')(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max2')(inner)

    conv_to_rnn_dims = (img_w // (pool_size ** 2), (img_h // (pool_size ** 2)) * conv_filters)
    inner = Reshape(target_shape=conv_to_rnn_dims, name='reshape')(inner)

    # cuts down input size going into RNN:
    inner = Dense(time_dense_size, activation=act, name='dense1')(inner)

    # Two layers of bidirectional GRUs
    # GRU seems to work as well, if not better than LSTM:
    gru_1 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru1')(inner)
    gru_1b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru1_b')(inner)
    gru1_merged = add([gru_1, gru_1b])
    gru_2 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru2')(gru1_merged)
    gru_2b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru2_b')(gru1_merged)

    # transforms RNN output to character activations:
    inner = Dense(get_output_size(), kernel_initializer='he_normal',
                  name='dense2')(concatenate([gru_2, gru_2b]))
    y_pred = Activation('softmax', name='softmax')(inner)
    Model(inputs=input_data, outputs=y_pred).summary()

    labels = Input(name='the_labels', shape=[absolute_max_string_len], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')
    # Keras doesn't currently support loss funcs with extra parameters
    # so CTC loss is implemented in a lambda layer
    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([y_pred, labels, input_length, label_length])

    # clipnorm seems to speeds up convergence
    sgd = SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True, clipnorm=5)

    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=[loss_out,y_pred])

    # the loss calc occurs elsewhere, so use a dummy lambda func for the loss
    model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=sgd)
    # if start_epoch > 0:
    #     weight_file = os.path.join(OUTPUT_DIR, os.path.join(run_name, 'weights%02d.h5' % (start_epoch - 1)))
    #     model.load_weights(weight_file)
    # captures output of softmax so we can decode the output during visualization
    test_func = K.function([input_data], [y_pred])
    batch=get_batch(img_w,downsample_factor)

    for i in range(100000):
        print "Step",i


        inputs=batch[0]

        skip_batch=0
        # for label in batch[0]['source_str']:
        #     # print "LABELSS",label
        #     if len(label)<1:
        #         skip_batch=1
        # if skip_batch:
        #     continue



        # # SHOW BATCH CONTENT
        # for k in range(batch_size):
        #     input_debug=inputs['the_input'][k,:,:,0]
        #     im=Image.fromarray(input_debug*255)
        #     print inputs['source_str'][k]
        #     print inputs['the_labels'][k]
        #     im.show()
        #     raw_input()

        ctcloss=model.train_on_batch(inputs,y=np.zeros(batch_size))
        # print "LOSS",ctcloss

        if i % 10 == 0:
            print "Batch loss",ctcloss
            for label in batch[0]['source_str']:
                print "LABELS",label
            num_proc = min(batch[0]['the_input'].shape[0], 128)
            # print batch[0]['the_input'].shape
            decoded_res = decode_batch(test_func, batch[0]['the_input'][0:num_proc])
            print (decoded_res)
            for j in range(len(decoded_res)):
                decoded_res[j]="step_"+str(i)+"_word_"+str(j)+decoded_res[j]
            # save_images_batch(batch[0]['the_input'],decoded_res)


if __name__ == '__main__':
    run_name = datetime.datetime.now().strftime('%Y:%m:%d:%H:%M:%S')
    train(run_name, 0, 20, 1600)
    # increase to wider images and start at epoch 20. The learned weights are reloaded
    train(run_name, 20, 25, 512)
