# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 15:12:24 2016

@author: jtoledo
"""

import numpy as np
import re
def natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]
import sys
import os
from PIL import Image
import matplotlib.pyplot as plt
from keras.utils import np_utils
from random import shuffle
import re
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


batch_size=1
im_height=80
im_width=125
alphabet = 'abcdefghijklmnopqrstuvwxyz '
# define a mapping of chars to integers
char_to_int = dict((c, i) for i, c in enumerate(alphabet))

def text_to_ids(text):
    encoding=[]
    text=re.sub('[^a-zA-Z]+', '', text)
    for char in text:
        encoding.append(char_to_int[char.lower()])
    return np.array(encoding)

def ids_to_text(ids):
    text=""
    for i in ids:
        text=text+alphabet[i]
    return text
class EsposallesDataset():
    def __init__(self,BaseDir='/home/ntoledo/datasets/OfficialEsposalles',cvset='train',level='words'):
        self.BaseDir=BaseDir
        self.DataDir=BaseDir+'/'+cvset
        self.GroundTruth=self.DataDir+'/groundtruth_full.txt'
        self.level=level
        self.transcriptions={}
        self.categories={}
        self.persons={}
        with open(self.GroundTruth, mode='r') as infile:
            for line in infile:
                values=line.rstrip('\n').split(':',4)

                self.transcriptions[values[0]]=text_to_ids(values[1])
                self.categories[values[0]]=values[2]
                self.persons[values[0]]=values[3]
        self.categorydict={l:n for (n,l) in enumerate(['other','surname','name','location','occupation','state'])}
        self.revcategorydict={v:k for (k,v) in self.categorydict.iteritems()}
        self.persondict={l:n for (n,l) in enumerate(['none','other_person','husband','wife','husbands_father','husbands_mother','wifes_mother','wifes_father'])}
        self.revpersondict={v:k for (k,v) in self.persondict.iteritems()}
        self.generate_previous_categories_and_regdict()
        self.shuffled_registers=[k for k in self.reg_dict.iterkeys()]
        self.shuffled_registers.sort()
        self.register_iterator=iter(self.shuffled_registers)
        self.epoch_size = len(self.shuffled_registers)
        self.r_id=self.register_iterator.next()
        self.epoch=0

    def readNormalizedImage(self,imageid):
        # Read image from file
        v=imageid.split('_');
        filename=self.DataDir+'/'+'_'.join(v[0:2])+'/'+self.level+'/'+imageid+".png"
        im=Image.open(filename)


        #Resize image to fit maximum size
        maxsize = (im_height,im_width)
        im.thumbnail(maxsize)

        #Create background to paste image into
        background=np.zeros(maxsize)
        #average_bckg_color=int(np.mean(im[np.where(im<100)]))
        average_bckg_color = int(np.mean(np.array(im)))
        background.fill(average_bckg_color)
        background=Image.fromarray(background)

        #Paste image centered into grey background
        background.paste(im,box=(maxsize[1]/2-im.size[0]/2,maxsize[0]/2-im.size[1]/2))
        im=np.array(background)
        im= 1.-np.array(im)/255.

        return im


    def generate_previous_categories_and_regdict(self):
        l=self.categories.keys()
        l=sorted(l,key=natural_key)
        prevcategory=6
        prevpag,prevreg,pag,reg=(0,0,0,0)
        self.prevcategories={}
        self.reg_dict={}
        for s in l:
            # print s

            m=re.search('^idPage([0-9]{5})_Record([0-9]{1,2})_Line([0-9]{1,2})_Word([0-9]{1,2})$',s);
            if self.level=='words':
                im_id=s
            else:
                im_id="_".join(s.split('_')[0:3])

            pag,reg,lin,pos=int(m.group(1)),int(m.group(2)),int(m.group(3)),int(m.group(4))

            if self.reg_dict.has_key((pag,reg)):
               self.reg_dict[pag,reg].append(im_id)
            else:
               self.reg_dict[pag,reg]=[im_id]
            if prevpag==0 or prevpag==pag and prevreg == reg:
               self.prevcategories[im_id]=prevcategory
               prevcategories=self.categorydict[self.categories[im_id]]
            else:
               prevcategory=6
               self.prevcategories[im_id]=prevcategory
            prevpag=pag
            prevreg=reg

    def get_batch(self,show_word_ids=False):
        try:
            self.r_id=self.register_iterator.next()
        except StopIteration: #if no more register, shuffle and get first register again
            np.random.shuffle(self.shuffled_registers)
            self.register_iterator=iter(self.shuffled_registers)
            self.r_id=self.register_iterator.next()
            self.epoch+=1
        IDS=[i for i in self.reg_dict[self.r_id]]
        shuffle(IDS)
        
        X=[np.asarray(self.readNormalizedImage(i))[:,:,np.newaxis] for i in IDS ]
        category=[np.asarray(self.categorydict[self.categories[i]]) for i in IDS ]
        person=[np.asarray(self.persondict[self.persons[i]]) for i in IDS ]
        transcription=[self.transcriptions[i] for i in IDS]
        try:
            self.r_id=self.register_iterator.next()
        except StopIteration: #if no more register, shuffle and get first register again
            np.random.shuffle(self.shuffled_registers)
            self.register_iterator=iter(self.shuffled_registers)
            self.r_id=self.register_iterator.next()
            self.epoch+=1

        X=np.stack(X)
        X=X[np.newaxis,:,:,:,:]
        category=np_utils.to_categorical(category, len(self.categorydict.keys()))
        category=category[np.newaxis,:,:]
        person=np_utils.to_categorical(person, len(self.persondict.keys()))
        person=person[np.newaxis,:,:]

        return X,transcription,category,person,[IDS]


    def get_labels_from_categorical(self,ids,categories,persons=None):
        if persons is None:
            return [ zip(r_ids,[ self.revcategorydict[np.argmax(sample)] for sample in r_categories]) for r_ids,r_categories in zip (ids,categories)]
        return [ zip(r_ids,[ self.revcategorydict[np.argmax(sample)] for sample in r_categories],[ self.revpersondict[np.argmax(sample)] for sample in r_persons]) for r_ids,r_categories,r_persons in zip (ids,categories,persons)]

    def show_batch(self):

        ims,trans,cats,pers,names=self.get_batch()
        widths=[]
        heights=[]
        shapes=[]
        for i in range(len(ims[0,:,:,:,0])):
            im=ims[0,i,:,:,0]

            shapes.append(im.shape[0])
            im=255*im
            im=im.astype('uint8')
            print im.shape
            im=Image.fromarray(im)
            im.show()
            print trans[i]

            raw_input()
            #plt.imshow(im)



            #plt.show()
        print np.mean(shapes)
def main():
    E=EsposallesDataset()
    E.show_batch()

if __name__=="__main__":
    main()
