#! /usr/bin/env python

import argparse
import os
import cv2
import numpy as np
from preprocessing import parse_annotation
from utils import draw_boxes, GeneraXML
from frontend import YOLO
import json

#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"

argparser = argparse.ArgumentParser(
    description='Train and validate YOLO_v2 model on any dataset')

argparser.add_argument(
    '-c',
    '--conf',
    help='path to configuration file')

argparser.add_argument(
    '-w',
    '--weights',
    help='path to pretrained weights')

argparser.add_argument(
    '-i',
    '--image',
    help='path to an image')

def _main_(args):
 
    config_path  = args.conf
    weights_path = args.weights
    image_path   = args.image

    with open(config_path) as config_buffer:    
        config = json.load(config_buffer)

    ###############################
    #   Make the model 
    ###############################

    yolo = YOLO(architecture        = config['model']['architecture'],
                input_size          = config['model']['input_size'], 
                labels              = config['model']['labels'], 
                max_box_per_image   = config['model']['max_box_per_image'],
                anchors             = config['model']['anchors'])

    ###############################
    #   Load trained weights
    ###############################    

    print weights_path
    yolo.load_weights(weights_path)

    ###############################
    #   Predict bounding boxes 
    ###############################

    image = cv2.imread(image_path)
    #image = cv2.resize(image,(0,0),fx=0.6,fy=0.6);
    #image = image[350:766,350:766,:]
    #cv2.imwrite(image_path[:-4] + '_original' + image_path[-4:], image)
    #cv2.imshow("Entrada",image)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    h, w, c = image.shape
    new_h = 1536;
    new_w = 2048;
    image = cv2.resize(image,(new_w,new_h),fx=0.0,fy=0.0)
    ini_y = 0.0;
    ini_x = 0.0;
    
    boxes = yolo.predict(image)
    image = draw_boxes(image, boxes, config['model']['labels'])

    print len(boxes), 'boxes are found'

    #cv2.imwrite(image_path[:-4] + '_detected_1024' + image_path[-4:], image)
    GeneraXML(image_path,boxes,ini_y,ini_x,new_w,new_h,w,h)
    

if __name__ == '__main__':
    args = argparser.parse_args()
    _main_(args)
