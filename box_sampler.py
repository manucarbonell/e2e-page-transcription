''' BOX SAMPLER:
(see box sampler helper in densecap's implementation)

input: list of two lists:
the first list contains data about the input boxes, 
the second list contains data about the target boxes.

returns a list of three lists:
first list contains data about positive input boxes.
second list contains data about target boxes corresponding to positive input boxes.
third list contains data about negative input boxes. '''

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import utils

class BoxSampler(Layer):

    def __init__(self, region_proposal_data, output_dim, **kwargs):
        self.output_dim = output_dim
	self.region_proposal_data = region_proposal_data
        super(MyLayer, self).__init__(**kwargs)

    def build(self, input_shape):
	self.input_data = self.region_proposal_data[0]
	self.target_data = self.region_proposal_data[1]
	
	self.input_boxes = self.input_data[0]

	self.target_boxes = self.target_data[0]
	
	idxs = self.box_sampler(self.input_boxes,self.target_boxes)
	
	
        '''# Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(input_shape[1], self.output_dim),
                                      initializer='uniform',
                                      trainable=True)'''
        super(BoxSampler, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        return K.dot(x, self.kernel)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)


    def sample_boxes(self.input_boxes,self.target_boxes):
	"(see densecap's box sampler module)"
	input_boxes = self.input_boxes
	target_boxes = self.target_boxes

	low_thresh = 0.3
	high_thresh = 0.7

 	ious = utils.bbox_iou(input_boxes,target_boxes)
	
	# to be continued	
	
	

