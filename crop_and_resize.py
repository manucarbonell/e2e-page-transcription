from keras.engine import Layer

class Crop_and_resize (Layer):
    def __init__(self, bins):
        self.bins = bins
        super(Crop_and_resize,self).__init__()
    def build(self,input_shape):
        self.ax=input_shape[-2]
        self.ay=input_shape[-1]
        super(Crop_and_resize,self).__init__()
    def call(self, [x, boxes], mask=None):
        output=[]
        # generate boxes from output_yolo
    def compute_output_shape(self,input_shape):
        return tuple((input_shape[0],sum([ a*b for (a,b) in self.bins])*input_shape[-3]))
