import numpy as np
import os
import xml.etree.ElementTree as ET
import tensorflow as tf
import copy
import cv2
import pagexml

class BoundBox:
    def __init__(self, x, y, w, h, xbl=0,yblu=0,ybld=0,xbr=0,ybru=0,ybrd=0,h0=0,h1=0, c = None, classes = None):
        self.x     = x
        self.y     = y
        self.w     = w
        self.h     = h
	self.xbl   = xbl
	self.yblu  = yblu
	self.ybld  = ybld
	self.xbr   = xbr
	self.ybru  = ybru
	self.ybrd  = ybrd
        self.h0    = h0
        self.h1    = h1
        
        self.c     = c
        self.classes = classes

        self.label = -1
        self.score = -1

    def get_label(self):
        if self.label == -1:
            self.label = np.argmax(self.classes)
        
        return self.label
    
    def get_score(self):
        if self.score == -1:
            self.score = self.classes[self.get_label()]
            
        return self.score

class WeightReader:
    def __init__(self, weight_file):
        self.offset = 4
        self.all_weights = np.fromfile(weight_file, dtype='float32')
        
    def read_bytes(self, size):
        self.offset = self.offset + size
        return self.all_weights[self.offset-size:self.offset]
    
    def reset(self):
        self.offset = 4

def normalize(image):
    image = image / 255.
    
    return image

def bbox_iou(box1, box2):
    x1_min  = box1.x - box1.w/2
    x1_max  = box1.x + box1.w/2
    y1_min  = box1.y - box1.h/2
    y1_max  = box1.y + box1.h/2
    
    x2_min  = box2.x - box2.w/2
    x2_max  = box2.x + box2.w/2
    y2_min  = box2.y - box2.h/2
    y2_max  = box2.y + box2.h/2
    
    intersect_w = interval_overlap([x1_min, x1_max], [x2_min, x2_max])
    intersect_h = interval_overlap([y1_min, y1_max], [y2_min, y2_max])
    
    intersect = intersect_w * intersect_h
    
    union = box1.w * box1.h + box2.w * box2.h - intersect
    
    return float(intersect) / union
    
def interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2,x4) - x1
    else:
        if x2 < x3:
            return 0
        else:
            return min(x2,x4) - x3  

def draw_boxes(image, boxes, labels):
    
    for box in boxes:
        xmin  = int((box.x - box.w/2) * image.shape[1])
        xmax  = int((box.x + box.w/2) * image.shape[1])
        ymin  = int((box.y - box.h/2) * image.shape[0])
        ymax  = int((box.y + box.h/2) * image.shape[0])
	xmin  = max(0,xmin)
	xmax  = min(image.shape[1],xmax)
	ymin  = max(0,ymin)
	ymax  = min(image.shape[0],ymax)
	xbl1  = int((box.x*image.shape[1])-box.xbl*32)
	xbl2  = int((box.x*image.shape[1])+box.xbr*32)
	ybl1  = 0
	ybl2  = 0
	box.yblu = max(0.0,box.yblu)
	box.ybld = max(0.0,box.ybld)
	box.ybru = max(0.0,box.ybru)
	box.ybrd = max(0.0,box.ybrd)
	print(box.xbl,box.yblu,box.ybld,box.xbr,box.ybrd,box.ybld)
	if max(box.ybld*32,box.yblu*32) == box.ybld*32:
		ybl1  = int((box.y*image.shape[0])-box.ybld)
	else:
		ybl1  = int((box.y*image.shape[0])+box.yblu*32)
	
	if max(box.ybrd*32,box.ybru*32) == box.ybrd*32:
		ybl2  = int((box.y*image.shape[0])-box.ybrd*32)
	else:
		ybl2  = int((box.y*image.shape[0])+box.ybru*32)
	xbl1 = max(0,xbl1)
	ybl1 = max(0,ybl1)
	xbl2 = min(image.shape[1],xbl2)
	ybl2 = min(image.shape[0],ybl2)
	#print(xbl1,xbl2,box.y*image.shape[0],box.ybld,box.yblu,box.ybrd,box.ybru)
	#raw_input('Press a key')
        #cv2.rectangle(image, (xmin,ymin), (xmax,ymax), (0,255,0), 3)
        #cv2.putText(image, 
        #            labels[box.get_label()] + ' ' + str(box.get_score()), 
        #            (xmin, ymin - 3), 
        #            cv2.FONT_HERSHEY_SIMPLEX, 
        #            (1e-4 * image.shape[0])*5, 
        #            (0,255,0), 2) # ymin-13
	cv2.line(image,(xbl1,ybl1),(xbl2,ybl2),(255,0,0),1)
        
    return image        
        
def decode_netout(netout, obj_threshold, nms_threshold, anchors, nb_class):
    grid_h, grid_w, nb_box = netout.shape[:3]

    boxes = []
    
    # decode the output by the network
    netout[..., 4]  = sigmoid(netout[..., 4])
    netout[..., 5:] = netout[..., 4][..., np.newaxis] * softmax(netout[..., 5:])
    netout[..., 5:] *= netout[..., 5:] > obj_threshold
    
    for row in range(grid_h):
        for col in range(grid_w):
            for b in range(nb_box):
                # from 4th element onwards are confidence and class classes
                classes = netout[row,col,b,5:]
                
                if np.sum(classes) > 0:
                    # first 4 elements are x, y, w, and h
                    x, y, w, h = netout[row,col,b,:4]

                    x = (col + sigmoid(x)) / grid_w # center position, unit: image width
                    y = (row + sigmoid(y)) / grid_h # center position, unit: image height
                    w = anchors[2 * b + 0] * np.exp(w) / grid_w # unit: image width
                    h = anchors[2 * b + 1] * np.exp(h) / grid_h # unit: image height
                    confidence = netout[row,col,b,4]
                    
                    box = BoundBox(x, y, w, h, confidence, classes)
                    
                    boxes.append(box)

    # suppress non-maximal boxes
    for c in range(nb_class):
        sorted_indices = list(reversed(np.argsort([box.classes[c] for box in boxes])))

        for i in xrange(len(sorted_indices)):
            index_i = sorted_indices[i]
            
            if boxes[index_i].classes[c] == 0: 
                continue
            else:
                for j in xrange(i+1, len(sorted_indices)):
                    index_j = sorted_indices[j]
                    
                    if bbox_iou(boxes[index_i], boxes[index_j]) >= nms_threshold:
                        boxes[index_j].classes[c] = 0
                        
    # remove the boxes which are less likely than a obj_threshold
    boxes = [box for box in boxes if box.get_score() > obj_threshold]
    
    return boxes
def GeneraXML(image_path,boxes,ini_y,ini_x,new_w,new_h,w,h):
	pxml = pagexml.PageXML()
	values = image_path.split('/')
	image = cv2.imread(image_path)
	pxml.newXml("yolo_base_line_detection_v1", values[-1], w, h)
	
	page = pxml.selectNth("//_:Page",0)
	reg = pxml.addTextRegion(page)
	pxml.setCoordsBBox(reg, 0, 0, w, h)
	conf = pagexml.ptr_double()
	for box in boxes:
		xbl1  = (box.x*2048)-box.xbl*32
		xbl2  = (box.x*2048)+box.xbr*32
		ybl1  = 0
		ybl2  = 0
		box.yblu = max(0.0,box.yblu)
		box.ybld = max(0.0,box.ybld)
		box.ybru = max(0.0,box.ybru)
		box.ybrd = max(0.0,box.ybrd)
		print(box.xbl,box.yblu,box.ybld,box.xbr,box.ybrd,box.ybld)
		if max(box.ybld*32.0,box.yblu*32.0) == box.ybld*32.0:
			ybl1  = (box.y*1536.0)-box.ybld*32.0
		else:
			ybl1  = (box.y*1536.0)+box.yblu*32.0

		if max(box.ybrd*32,box.ybru*32.0) == box.ybrd*32.0:
			ybl2  = (box.y*1536.0)-box.ybrd*32.0
		else:
			ybl2  = (box.y*1536.0)+box.ybru*32.0

		xbl1 = max(0.0,xbl1)
		ybl1 = max(0.0,ybl1)
		xbl2 = min(2048.0,xbl2)
		ybl2 = min(1536.0,ybl2)
		if ini_y > 0.0:
			ybl1 = ybl1-ini_y;
			ybl2 = ybl2-ini_y;
		xbl1 = xbl1 / (new_w*1.0);
		xbl2 = xbl2 / (new_w*1.0);
		ybl1 = ybl1 / (new_h*1.0);
		ybl2 = ybl2 / (new_h*1.0);
		xbl1 = xbl1 * w*1.0
		xbl2 = xbl2 * w*1.0
		ybl1 = ybl1 * h*1.0
		ybl2 = ybl2 * h*1.0
                h1 = (box.h0+box.h1)*32.0
		line = pxml.addTextLine(reg)
		conf.assign(float(box.c))
                print(xbl1,ybl1,xbl2,ybl2)
                cv2.line(image,(int(xbl1),int(ybl1)),(int(xbl2),int(ybl2)),(255,0,0),3)
		pxml.setBaseline(line, xbl1, ybl1, xbl2, ybl2, conf)
		pxml.setPolystripe(line, h1)
        cv2.namedWindow("Linees",0)
        cv2.imshow("Linees",image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
	file_xml = image_path[:-4]+".xml"
	pxml.write(file_xml)
def sigmoid(x):
    return 1. / (1. + np.exp(-x))

def softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)
    
    if np.min(x) < t:
        x = x/np.min(x)*t
        
    e_x = np.exp(x)
    
    return e_x / e_x.sum(axis, keepdims=True)
