import xml.etree.ElementTree as ET
import fnmatch
import os
import sys
import shutil
import numpy as np
from shutil import copyfile
import json
from PIL import Image


def create_input_dir_and_ids():
    image_ids=[]
    matches = []
    image_id=0
    basewidth =2048
    for root, dirnames, filenames in os.walk('/home/mcarbonell/Documents/DATASETS/gt-ancestry'): ## Folder where we have the xmls of the ancestry_gt
        for filename in fnmatch.filter(filenames, '*.jpg'):
            print "Process im with id",image_id
            image_id=image_id+1
            image_ids.append(os.path.splitext(filename)[0])
            matches.append(os.path.join(root, os.path.splitext(filename)[0]+'_gt.xml'))

            shutil.copy(os.path.join(root,filename),os.path.join(image_dir_out,str(image_id)+'.jpg'))
    return image_ids,matches

image_dir_out='ancestry-all'

if not os.path.exists(image_dir_out):
    os.mkdir(image_dir_out)

image_ids,matches=create_input_dir_and_ids()
filename_out='region_annotations.json'
n_image=0
images=[]

for m in matches:
    vals = m.split('/');
    filename_in = vals[-1]
    filename_in_wext = filename_in.split('.')[0]
    pathout = "/home/mcarbonell/Documents/DATASETS/gt-ancestry";

    tree = ET.parse(m);

    for el in tree.iter('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Page'):
        image={}
        n_region=0
        
        image['regions']=[]
        imagewidth = int(el.attrib['imageWidth']);
        imageheight = int(el.attrib['imageHeight']);
        idline = 1;
        xmin = 0.;
        ymin = 0.;
        xmax = 0.;
        ymax = 0.;
        xbl1 = 0.;
        ybl1 = 0.;
        xbl2 = 0.;
        ybl2 = 0.;
        label= "";

        for el in tree.iter('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextLine'):

            region={}
            for elc in el:
                box_transcription="<empty>"
                if elc.tag == "{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextEquiv":

                    for texteq in elc:
                        if texteq.tag== "{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Unicode":
                            box_transcription = texteq.text

                if elc.tag == "{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Coords":
                   pts_str = elc.attrib["points"];

                   vpts = pts_str.split(' ');

                   xmin=9999.9;ymin=99999.9;xmax=0.0;ymax=0.0
                   for pts in vpts:
                       ptvals = pts.split(',');
                       xval = float(ptvals[0])/(imagewidth*1.0);
                       yval = float(ptvals[1])/(imageheight*1.0);
                       xmin = min(xmin,xval);
                       ymin = min(ymin,yval);
                       xmax = max(xmax,xval);
                       ymax = max(ymax,yval);


            print(xmin*imagewidth,xmax*imagewidth,ymin*imageheight,ymax*imageheight)
            if xmin > xmax or ymin > ymax:
               raw_input()
            region['region_id']=n_region
            if box_transcription=="<empty>": continue
            n_region=n_region+1

            region['width']=int((float(xmax)-float(xmin))*imagewidth)
            region['height']=int((float(ymax)-float(ymin))*imageheight)
            region['x']=int(xmin*imagewidth)
            region['y']=int(ymin*imageheight)
            region['phrase']=box_transcription
            region['image_id']=image_ids.index("_".join(filename_in_wext.split('_')[:-1]))
            image['regions'].append(region)
            
        image['id']=image_ids.index("_".join(filename_in_wext.split('_')[:-1]))
        images.append(image)
fileout = open(filename_out,'w');
fileout.write(json.dumps(images))

fileout.close()
