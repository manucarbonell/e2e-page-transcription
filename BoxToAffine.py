'''
If the input image has height H and width W, then for each box (xc, yc, w, h)
we want to generate the following 2 x 3 affine transform matrix:
 [   h             2 * yc - H - 1 ]
 [  ---      0     -------------- ]
 [   H                  H - 1     ]
 [                                ]
 [           w     2 * xc - W - 1 ]
 [   0      ---    -------------- ]
 [           W          W - 1     ]

(distances are normalized with respect to page so H = W = 1 ) 
'''



from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf

class BoxToAffine(Layer):

    def __init__(self,output_dim,anchors, **kwargs):
        self.output_dim = output_dim
        self.anchors = anchors
        super(BoxToAffine, self).__init__(**kwargs)

    def build(self, input_shape):
	#B = input_shape[0]
	
	self.theta = K.zeros((input_shape[1],input_shape[2],input_shape[3],2,3))
        
        self.grid_w = input_shape[2]
        self.grid_h = input_shape[1]
	self.batch_size = input_shape[0]
	'''
        # Create a trainable weight variable for this layer.
        self.kernel = self.add_weight(name='kernel', 
                                      shape=(input_shape[1], self.output_dim),
                                      initializer='uniform',
                                      trainable=True)'''
        super(BoxToAffine, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self,input_data):
	
        cell_x = tf.to_float(tf.reshape(tf.tile(tf.range(self.grid_w), [self.grid_w]), (1, self.grid_w, self.grid_w, 1, 1)))
        #print "Shape cell_x",self.grid_w,self.grid_h
        cell_y = tf.transpose(cell_x, (0,2,1,3,4))

        cell_grid = tf.tile(tf.concat([cell_x,cell_y], -1), [1, 1, 1, 5, 1])
        cell_grid = cell_grid[:,0:self.grid_h,:,:,:]
        pred_box_xy = tf.sigmoid(input_data[..., :2]) + cell_grid

        ### adjust w and h
        pred_box_wh = tf.exp(input_data[..., 2:4]) * np.reshape(self.anchors, [1,1,1,5,2])
	
	
	xc = pred_box_xy[...,0]/(self.grid_w*1.0)
	
	yc = pred_box_xy[...,1]/(self.grid_h*1.0)

	w = pred_box_wh[...,0]/(self.grid_w*1.0)

	h = pred_box_wh[...,1]/(self.grid_h*1.0)

	#global theta1

        tetha1 = tf.concat([h,K.zeros(tf.shape(h)),2.0*yc-1.0],axis=-1)
        
        tetha1 = tf.reshape(tetha1,[1,48,64,5,3])
        
        tetha2 = tf.concat([K.zeros(tf.shape(w)),w,2.0*xc-1.0],axis=1)
        tetha2 = tf.reshape(tetha2,[1,48,64,5,3])
        
        self.theta = tf.stack([tetha1,tetha2],axis=-2)
	#print "THETA OUT"
	#tf.Print(self.theta)	
		
        return self.theta

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)
