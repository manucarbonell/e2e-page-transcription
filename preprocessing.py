import os
import cv2
import copy
import numpy as np
import imgaug as ia
from imgaug import augmenters as iaa
import xml.etree.ElementTree as ET
from utils import BoundBox, normalize, bbox_iou
import time
def parse_annotation(ann_dir, img_dir, labels=[]):
    all_imgs = []
    seen_labels = set()
    print "ANNDIR IM DIR",ann_dir,img_dir,
    for ann in sorted(os.listdir(ann_dir)):
        img = {'object':[]}

        tree = ET.parse(os.path.join(ann_dir,ann))

        for elem in tree.iter():
            if 'filename' in elem.tag:
                all_imgs += [img]
                img['filename'] = img_dir + elem.text
            if 'width' in elem.tag:
                img['width'] = int(elem.text)
            if 'height' in elem.tag:
                img['height'] = int(elem.text)
            if 'object' in elem.tag or 'part' in elem.tag:
                obj = {}

                for attr in list(elem):
                    if 'name' in attr.tag:
                        obj['name'] = attr.text
                        seen_labels.add(obj['name'])

                        if len(labels) > 0 and obj['name'] not in labels:
                            break
                        else:
                            img['object'] += [obj]

                    if 'bndbox' in attr.tag:
                        for dim in list(attr):
                            if 'xmin' in dim.tag:
                                obj['xmin'] = int(round(float(dim.text)))
                            if 'ymin' in dim.tag:
                                obj['ymin'] = int(round(float(dim.text)))
                            if 'xmax' in dim.tag:
                                obj['xmax'] = int(round(float(dim.text)))
                            if 'ymax' in dim.tag:
                                obj['ymax'] = int(round(float(dim.text)))

    return all_imgs, seen_labels

def parse_annotation_lines(ann_dir, img_dir, labels=[]):
    all_imgs = []
    seen_labels = set()
    print ann_dir,img_dir
    for ann in sorted(os.listdir(ann_dir)):

	img = {'object':[]}

	file_ann = open(os.path.join(ann_dir,ann),"r");
	lines = file_ann.readlines();
	file_ann.close();

	for l in lines:


		keys = l.split();

		if keys[0]=='filename':
			all_imgs += [img]
			#print(keys[1]);
			img['filename'] = img_dir+keys[1];
		if keys[0]=='width':
			img['width'] = int(keys[1]);
		if keys[0]=='height':
			img['height'] = int(keys[1]);
		if keys[0]=='BB':
			obj = {}
			obj['name'] = 'line';
			seen_labels.add(obj['name'])
			img['object'] += [obj];
			obj['xmin'] = float(keys[1]);
			obj['ymin'] = float(keys[2]);
			obj['xmax'] = float(keys[3]);
			obj['ymax'] = float(keys[4]);
                        obj['xbl1'] = float(keys[5]);
			obj['ybl1'] = float(keys[6]);
			obj['xbl2'] = float(keys[7]);
			obj['ybl2'] = float(keys[8]);
			obj['h0'] = float(keys[9]);
			obj['h1'] = float(keys[10]);

    return all_imgs, seen_labels

class BatchGenerator:
    def __init__(self, images,
                       config,
                       shuffle=True,
                       jitter=True,
                       norm=None):
        self.generator = None

        self.images = images
        self.config = config

        self.shuffle = shuffle
        self.jitter  = jitter
        self.norm    = norm

        self.anchors = [BoundBox(0, 0, config['ANCHORS'][2*i], config['ANCHORS'][2*i+1]) for i in range(len(config['ANCHORS'])/2)]

        ### augmentors by https://github.com/aleju/imgaug
        sometimes = lambda aug: iaa.Sometimes(0.5, aug)

        # Define our sequence of augmentation steps that will be applied to every image
        # All augmenters with per_channel=0.5 will sample one value _per image_
        # in 50% of all cases. In all other cases they will sample new values
        # _per channel_.
        self.aug_pipe = iaa.Sequential(
            [
                # apply the following augmenters to most images
                #iaa.Fliplr(0.5), # horizontally flip 50% of all images
                #iaa.Flipud(0.2), # vertically flip 20% of all images
                #sometimes(iaa.Crop(percent=(0, 0.1))), # crop images by 0-10% of their height/width
                sometimes(iaa.Affine(
                    #scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}, # scale images to 80-120% of their size, individually per axis
                    #translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)}, # translate by -20 to +20 percent (per axis)
                    #rotate=(-5, 5), # rotate by -45 to +45 degrees
                    #shear=(-5, 5), # shear by -16 to +16 degrees
                    #order=[0, 1], # use nearest neighbour or bilinear interpolation (fast)
                    #cval=(0, 255), # if mode is constant, use a cval between 0 and 255
                    #mode=ia.ALL # use any of scikit-image's warping modes (see 2nd image from the top for examples)
                )),
                # execute 0 to 5 of the following (less important) augmenters per image
                # don't execute all of them, as that would often be way too strong
                iaa.SomeOf((0, 5),
                    [
                        #sometimes(iaa.Superpixels(p_replace=(0, 1.0), n_segments=(20, 200))), # convert images into their superpixel representation
                        iaa.OneOf([
                            iaa.GaussianBlur((0, 3.0)), # blur images with a sigma between 0 and 3.0
                            iaa.AverageBlur(k=(2, 7)), # blur image using local means with kernel sizes between 2 and 7
                            iaa.MedianBlur(k=(3, 11)), # blur image using local medians with kernel sizes between 2 and 7
                        ]),
                        iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)), # sharpen images
                        #iaa.Emboss(alpha=(0, 1.0), strength=(0, 2.0)), # emboss images
                        # search either for all edges or for directed edges
                        #sometimes(iaa.OneOf([
                        #    iaa.EdgeDetect(alpha=(0, 0.7)),
                        #    iaa.DirectedEdgeDetect(alpha=(0, 0.7), direction=(0.0, 1.0)),
                        #])),
                        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5), # add gaussian noise to images
                        iaa.OneOf([
                            iaa.Dropout((0.01, 0.1), per_channel=0.5), # randomly remove up to 10% of the pixels
                            #iaa.CoarseDropout((0.03, 0.15), size_percent=(0.02, 0.05), per_channel=0.2),
                        ]),
                        #iaa.Invert(0.05, per_channel=True), # invert color channels
                        iaa.Add((-10, 10), per_channel=0.5), # change brightness of images (by -10 to 10 of original value)
                        iaa.Multiply((0.5, 1.5), per_channel=0.5), # change brightness of images (50-150% of original value)
                        iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5), # improve or worsen the contrast
                        #iaa.Grayscale(alpha=(0.0, 1.0)),
                        #sometimes(iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)), # move pixels locally around (with random strengths)
                        #sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.05))) # sometimes move parts of the image around
                    ],
                    random_order=True
                )
            ],
            random_order=True
        )

        if shuffle: np.random.shuffle(self.images)

    def get_generator(self):
        if self.generator == None:
            self.generator = self.make_generator()

        return self.generator

    def make_generator(self):
        num_img = len(self.images)

        total_count = 0
        batch_count = 0

        x_batch = np.zeros((self.config['BATCH_SIZE'], self.config['IMAGE_H'], self.config['IMAGE_W'], 3))                         # input images
        b_batch = np.zeros((self.config['BATCH_SIZE'], 1     , 1     , 1    ,  self.config['TRUE_BOX_BUFFER'], 12))   # list of self.config['TRUE_self.config['BOX']_BUFFER'] GT boxes
        y_batch = np.zeros((self.config['BATCH_SIZE'], self.config['GRID_H'],  (self.config['GRID_W']), self.config['BOX'], 12+1+1))                # desired network output

        while True:
            if total_count < num_img:
                train_instance = self.images[total_count]

		for pr in range(1):
		        # augment input image and fix object's position and size
		        img, all_objs = self.aug_image(train_instance, jitter=self.jitter)

		        # construct output from object's x, y, w, h
		        true_box_index = 0
		        #hits = np.zeros(5);
		        for obj in all_objs:
		            if obj['xmax'] > obj['xmin'] and obj['ymax'] > obj['ymin'] and obj['name'] in self.config['LABELS']:
		                center_x = .5*(obj['xmin'] + obj['xmax'])
				center_y = .5*(obj['ymin'] + obj['ymax'])
				lix = abs(center_x-obj['xbl1'])
				liy = center_y-obj['ybl1']
				liyu = 0.;
				liyd = 0.;
				if liy < 0:
					liyu = abs(liy);
				else:
					liyd = liy;
				rix = abs(center_x-obj['xbl2'])
				riy = center_y-obj['ybl2']
				riyu = 0.;
				riyd = 0.;
				if riy < 0:
					riyu = abs(riy);
				else:
					riyd = riy;

				center_x = center_x / (float(self.config['IMAGE_W']) / self.config['GRID_W'])
                                center_y = center_y / (float(self.config['IMAGE_H']) / self.config['GRID_H'])

				lix = lix / (float(self.config['IMAGE_W']) / self.config['GRID_W']);
				liyu = liyu / (float(self.config['IMAGE_W']) / self.config['GRID_W']);
				liyd = liyd / (float(self.config['IMAGE_W']) / self.config['GRID_W']);

				rix = rix / (float(self.config['IMAGE_W']) / self.config['GRID_W']);
				riyu = riyu / (float(self.config['IMAGE_H']) / self.config['GRID_H']);
				riyd = riyd / (float(self.config['IMAGE_H']) / self.config['GRID_H']);
				h0 = obj['h0'] / (float(self.config['IMAGE_H']) / self.config['GRID_H']);
				h1 = obj['h1'] / (float(self.config['IMAGE_H']) / self.config['GRID_H']);
				#print(lix,liyu,liyd,rix,riyu,riyd)

		                grid_x = int(np.floor(center_x))
		                grid_y = int(np.floor(center_y))

		                if grid_x < self.config['GRID_W'] and grid_y < self.config['GRID_H']:
		                    obj_indx  = self.config['LABELS'].index(obj['name'])

		                    center_w = (obj['xmax'] - obj['xmin']) / (float(self.config['IMAGE_W']) / self.config['GRID_W']) # unit: grid cell
				    #center_w = (obj['xmax'] - obj['xmin']) / (float(self.config['IMAGE_W']) / self.config['GRID_W']) # unit: grid cell
		                    center_h = (obj['ymax'] - obj['ymin']) / (float(self.config['IMAGE_H']) / self.config['GRID_H']) # unit: grid cell
		                    #center_h = (obj['ymax'] - obj['ymin']) / self.config['GRID_W']) # unit: grid cell
		                    box = [center_x, center_y, center_w, center_h]
				    linc = [lix, liyu, liyd]
				    rinc = [rix, riyu, riyd]
				    hinc = [h0, h1]
		                    # find the anchor that best predicts this box
		                    best_anchor = -1
		                    max_iou     = -1

		                    shifted_box = BoundBox(0,
		                                           0,
		                                           center_w,
		                                           center_h)

		                    for i in range(len(self.anchors)):
		                        anchor = self.anchors[i]
		                        iou    = bbox_iou(shifted_box, anchor)

		                        if max_iou < iou:
		                            best_anchor = i
		                            max_iou     = iou
		                    #hits[best_anchor] += 1;
		                    # assign ground truth x, y, w, h, confidence and class probs to y_batch
		                    y_batch[batch_count, grid_y, grid_x, best_anchor, 0:12] = box+linc+rinc+hinc
				    y_batch[batch_count, grid_y, grid_x, best_anchor, 12  ] = 1.
		                    y_batch[batch_count, grid_y, grid_x, best_anchor, 13  ] = obj_indx

		                    # assign the true box to b_batch
		                    b_batch[batch_count, 0, 0, 0, true_box_index, 0:12] = box+linc+rinc+hinc

		                    true_box_index += 1
		                    true_box_index = true_box_index % self.config['TRUE_BOX_BUFFER']
		        #print(hits)
		        # assign input image to x_batch
		        if self.norm != None:
		            x_batch[batch_count] = self.norm(img)
		        else:
		            # plot image and bounding boxes for sanity check
		            for obj in all_objs:
		                if obj['xmax'] > obj['xmin'] and obj['ymax'] > obj['ymin']:
		                    cv2.rectangle(img[:,:,::-1], (obj['xmin'],obj['ymin']), (obj['xmax'],obj['ymax']), (255,0,0), 3)
		                    cv2.putText(img[:,:,::-1], obj['name'],
		                                (obj['xmin']+2, obj['ymin']+12),
		                                0, 1.2e-3 * img.shape[0],
		                                (0,255,0), 2)

		            x_batch[batch_count] = img

		        # increase instance counter in current batch
		        batch_count += 1

		if batch_count >= self.config['BATCH_SIZE']:
		        yield [x_batch, b_batch], y_batch

		        x_batch = np.zeros((self.config['BATCH_SIZE'], self.config['IMAGE_H'], self.config['IMAGE_W'], 3))
		        y_batch = np.zeros((self.config['BATCH_SIZE'], self.config['GRID_H'],  (self.config['GRID_W']),  self.config['BOX'], 13+self.config['CLASS']))
		        b_batch = np.zeros((self.config['BATCH_SIZE'], 1     , 1     , 1    ,  self.config['TRUE_BOX_BUFFER'], 12))
		        batch_count = 0
            total_count += 1
            if total_count >= num_img:
                total_count = 0
                if self.shuffle: np.random.shuffle(self.images)

    def aug_image(self, train_instance, jitter):
	image_name = train_instance['filename'];
        # print(image_name)
	image = cv2.imread(image_name)
	h, w, c = image.shape
	new_h = 1536;
	new_w = 2048;
	image = cv2.resize(image,(new_w,new_h),fx=0.0,fy=0.0,interpolation=cv2.INTER_AREA)
	ini_x = 0;
        ini_y = 0;
	all_objs = copy.deepcopy(train_instance['object'])
	h, w, c = image.shape
	#image2 = image;
	if jitter:
	    ### scale the image
	    #scale = np.random.uniform() / 10. + 1.
	    #image = cv2.resize(image, (0,0), fx = scale, fy = scale)

	    ### translate the image
	    #max_offx = (scale-1.) * w
	    #max_offy = (scale-1.) * h
	    #offx = int(np.random.uniform() * max_offx)
	    #offy = int(np.random.uniform() * max_offy)

	    #image = image[offy : (offy + h), offx : (offx + w)]

	    ### flip the image
	    #flip = np.random.binomial(1, .5)
	    #if flip > 0.5: image = cv2.flip(image, 1)

	    image = self.aug_pipe.augment_image(image)



	#print(self.config['IMAGE_W'])
	#print(self.config['IMAGE_H'])
	# fix object's position and size

	#print(len(all_objs))
	for obj in all_objs:
		#print(obj)
		#print(new_w,new_h)
		#print(ini_x,ini_y)

		for attr in ['xmin', 'xmax', 'xbl1', 'xbl2']:
			obj[attr] = int((obj[attr]*new_w)+ini_x)
			#if jitter: obj[attr] = int(obj[attr] * scale - offx)

			#obj[attr] = int(obj[attr] * float(self.config['IMAGE_W']) / w)
			#obj[attr] = max(min(obj[attr], self.config['IMAGE_W']), 0) #0

		for attr in ['ymin', 'ymax', 'ybl1', 'ybl2']:
			obj[attr] = int((obj[attr]*new_h)+ini_y)
			#if jitter: obj[attr] = int(obj[attr] * scale - offy)

			#obj[attr] = int(obj[attr] * float(self.config['IMAGE_H']) / h)
			#obj[attr] = max(min(obj[attr], self.config['IMAGE_H']), 0) #0
		#print(obj)
		for attr in ['h0','h1']:
			obj[attr] = int((obj[attr]*new_h))
			#if jitter: obj[attr] = int(obj[attr] * scale - offy)

		#if jitter and flip > 0.5:
		#	xmin = obj['xmin']
		#	obj['xmin'] = self.config['IMAGE_W'] - obj['xmax']
		#	obj['xmax'] = self.config['IMAGE_W'] - xmin
		#print(obj)
                #cv2.rectangle(image2,(obj['xmin'],obj['ymin']),(obj['xmax'],obj['ymax']),(255,0,0),3);
                #cv2.line(image2,(obj['xbl1'],obj['ybl1']),(obj['xbl2'],obj['ybl2']),(0,255,0),3);
        #cv2.namedWindow("Imatge Capsetes",0)
        #cv2.imshow("Imatge Capsetes",image2)
        #cv2.waitKey(0);
        #cv2.destroyAllWindows()
	return image, all_objs

    def aug_image_old(self, train_instance, jitter):
        image_name = train_instance['filename']
	print(image_name);
	image = cv2.imread(image_name)
	image = cv2.resize(image,(0,0),fx=0.6,fy=0.6)
        h, w, c = image.shape
        #print("Shape ",image.shape)
        all_objs = copy.deepcopy(train_instance['object'])
	#all_objs_new = copy.deepcopy(train_instance['object'])
        if jitter:
            ### scale the image
            scale = np.random.uniform() / 10. + 1.
            image = cv2.resize(image, (0,0), fx = scale, fy = scale)

            ### translate the image
            max_offx = (scale-1.) * w
            max_offy = (scale-1.) * h
            offx = int(np.random.uniform() * max_offx)
            offy = int(np.random.uniform() * max_offy)

            image = image[offy : (offy + h), offx : (offx + w)]

            ### flip the image
            flip = np.random.binomial(1, .5)
            if flip > 0.5: image = cv2.flip(image, 1)

            image = self.aug_pipe.augment_image(image)

        # resize the image to standard size
        #image = cv2.resize(image, (self.config['IMAGE_H'], self.config['IMAGE_W']))

	ptx = int(np.random.uniform(300,image.shape[1]-300))
	pty = int(np.random.uniform(300,image.shape[0]-300))


	image = image[pty-208:pty+208,ptx-208:ptx+208,::-1]
	print(self.config['IMAGE_W'])
	print(self.config['IMAGE_H'])
	# fix object's position and size
	objs_to_remove = [];
	print(len(all_objs))
	for obj in all_objs:
	    print(obj)
	    print(ptx,pty)
	    for attr in ['xmin', 'xmax']:
	        if jitter: obj[attr] = int(obj[attr] * scale - offx)

	        obj[attr] = int(obj[attr] * float(self.config['IMAGE_W']) / w)
	        obj[attr] = max(min(obj[attr], self.config['IMAGE_W']), 0) #0


	    for attr in ['ymin', 'ymax']:
	        if jitter: obj[attr] = int(obj[attr] * scale - offy)

	        obj[attr] = int(obj[attr] * float(self.config['IMAGE_H']) / h)
	    	obj[attr] = max(min(obj[attr], self.config['IMAGE_H']), 0) #0
	    print(obj)

	    if jitter and flip > 0.5:
	        xmin = obj['xmin']
	        obj['xmin'] = self.config['IMAGE_W'] - obj['xmax']
	        obj['xmax'] = self.config['IMAGE_W'] - xmin

	    entra=False;
	    if obj['xmax'] < (ptx-208) :
	    	entra = True;
 	    if	obj['xmin'] > (ptx+208) :
	    	entra = True;
	    if obj['ymax'] < (pty-208) :
	    	entra = True;
 	    if	obj['ymin'] > (pty+208) :
	    	entra = True;
	    obj['xmin'] = max(0,obj['xmin']-(ptx-208));
	    obj['xmax'] = min(416,obj['xmax']-(ptx-208));
	    obj['ymin'] = max(0,obj['ymin']-(pty-208));
	    obj['ymax'] = min(416,obj['ymax']-(pty-208));

	    if entra:
		#b = all_objs.index(obj)
		objs_to_remove += [obj];
	    else:
		print(obj)
	for obj in objs_to_remove:
		all_objs.remove(obj);
	print(len(all_objs))

        return image, all_objs

    def get_dateset_size(self):
        return int(np.ceil(float(len(self.images))/self.config['BATCH_SIZE']))
